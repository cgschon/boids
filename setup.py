#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 10:55:04 2017

@author: ChristopherSchon
"""

from setuptools import setup, find_packages

setup(
	  name = "Boids", version = "1.0.0", \
       author = 'Christopher Schon', \
       author_email = 'chris.g.schon@gmail.com', \
       url = 'https://bitbucket.org/cgschon/boids/', \
      packages = find_packages(exclude=['*test']), \
      scripts = ['scripts/boids'], \
      install_requires = ['argparse', 'numpy', 'matplotlib','scipy', 'yaml','nose', 'dill']
    )
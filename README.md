boids program version 1.0.0 
1/2/2017

--------------------------------------------------------------

Hello

------------------------------------------------------------------

This package is a refactorisation of Boids model
(http://dl.acm.org/citation.cfm?doid=37401.37406) code
provided by the course lecturers for the
second assessment of Research Software
Engineering with Python. 

------------------------------------------------------------------

GENERAL USAGE

The boids animation is created from the command line with (e.g.):

scripts/boids --frames 100 --interval 100 --midweight 0.01 
--nearbyradius 100 --numboids 50 --speedradius 10000 
--speedweight 0.125 --xlims -1000 2500 --xposrange -450 50 
--xvelrange 0 10 --ylims -500 1500 --yposrange 300 600 --yvelrange -2 2

All arguments are optional, and default to the original code's values.

--------------------------------------------------------------

(C) University College London 2017

This 'boids' package is granted into the public domain.
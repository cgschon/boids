#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 18 13:16:29 2017

@author: ChristopherSchon
"""

from argparse import ArgumentParser
from Boids.visualise_boids import visualise_boids as visualise_boids


def process():
    parser = ArgumentParser(description = "Generate desired boids arguments")

    parser.add_argument('--frames', '--f', default = 50)
    parser.add_argument('--interval', '--i', default = 50)
    parser.add_argument('--numboids', '--n', default = 50)
    parser.add_argument('--xposrange', nargs = 2, default = [-450,50])
    parser.add_argument('--yposrange', nargs = 2, default = [300,600])
    parser.add_argument('--xvelrange', nargs = 2, default = [0,10])
    parser.add_argument('--yvelrange', nargs = 2, default = [-20,20])
    parser.add_argument('--xlims', nargs = 2, default = (-500,1500))
    parser.add_argument('--ylims', nargs = 2, default = (-500,1500))
    
    parser.add_argument('--midweight',default = 0.01)
    parser.add_argument('--nearbyradius', default = 100)
    parser.add_argument('--speedradius', default = 10000)
    parser.add_argument('--speedweight', default = 0.125)
    
    arguments = parser.parse_args()
    def mapargs(arg, t):
        return(map(t, arg))
    
    visualise_boids(int(arguments.frames), int(arguments.interval), int(arguments.numboids),\
            list(mapargs(arguments.xposrange, float)), list(mapargs(arguments.yposrange, float)),\
             list(mapargs(arguments.xvelrange, float)),list(mapargs(arguments.yvelrange, float)),
            tuple(mapargs(arguments.xlims, float)), tuple(mapargs(arguments.ylims, float)),\
            float(arguments.midweight), float(arguments.nearbyradius), float(arguments.speedradius), \
            float(arguments.speedweight)).run_animation()
    
    

    if __name__ == "__main__":
        process()
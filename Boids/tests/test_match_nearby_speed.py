#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 12:52:59 2017

@author: ChristopherSchon
"""
from Boids.boids import boids as boids
#from Boids.visualise_boids import visualise_boids as visualise_boids
from nose.tools import assert_equal
import os
import yaml
import numpy as np


def test_match_nearby_speed():
    method_str = 'match_nearby_speed'
    
    with open(os.path.join(os.path.dirname(__file__),'fixtures', 'test_num_boids.yaml')) as numboidf:
        fix_num_boids = yaml.load(numboidf)
        
    with open(os.path.join(os.path.dirname(__file__),'fixtures', 'test_position.yaml')) as pos:
        fix_position = yaml.load(pos)
        
    with open(os.path.join(os.path.dirname(__file__),'fixtures', 'test_velocity.yaml')) as vel:
        fix_velocity = yaml.load(vel)
    
    with open(os.path.join(os.path.dirname(__file__),'fixtures', method_str + '_sample.yaml')) as fixtures_file:
        fixture = yaml.load(fixtures_file)
    
    test = boids()
    test.num_boids = np.copy(fix_num_boids)
    test.position = np.copy(fix_position)
    test.velocity = np.copy(fix_velocity)
    test.match_nearby_speed(10000, 0.125)
    
    assert((test.velocity == fixture).all(), "match_nearby_speed does not give correct velocities for default inputs")
    
        
        
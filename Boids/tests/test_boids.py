#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 12:52:59 2017

@author: ChristopherSchon
"""
from Boids.boids import boids as boids
#from Boids.visualise_boids import visualise_boids as visualise_boids
from nose.tools import assert_equal


def test_boids():
    test = boids()
    print(test)
    assert_equal(set(list(test.__dict__.keys())), set(['position', 'num_boids', 'velocity']), \
                 "number boids, positions and velocities not initialised")
    assert_equal(int(len(test.__dict__['position'][0])), test.__dict__['num_boids'],\
                     "number of boids is not equal to number of positions"  )
    assert_equal(int(len(test.__dict__['velocity'][0])), test.__dict__['num_boids'],\
                     "number of boids is not equal to number of vel. vecs."  )
    
    
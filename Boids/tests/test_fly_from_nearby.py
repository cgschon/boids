#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 12:52:59 2017

@author: ChristopherSchon
"""
from Boids.boids import boids as boids
#from Boids.visualise_boids import visualise_boids as visualise_boids
from nose.tools import assert_equal
import os
import yaml
import numpy as np


def test_fly_from_nearby():
    method_str = 'fly_from_nearby'
    
    with open(os.path.join(os.path.dirname(__file__),'fixtures', 'test_num_boids.yaml')) as numboidf:
        num_boids = yaml.load(numboidf)
        
    with open(os.path.join(os.path.dirname(__file__),'fixtures', 'test_position.yaml')) as pos:
        position = yaml.load(pos)
        
    with open(os.path.join(os.path.dirname(__file__),'fixtures', 'test_velocity.yaml')) as vel:
        velocity = yaml.load(vel)
    
    with open(os.path.join(os.path.dirname(__file__),'fixtures', method_str + '_sample.yaml')) as fixtures_file:
        fixture = yaml.load(fixtures_file)
    
    test = boids()
    test.num_boids = np.copy(num_boids)
    test.position = np.copy(position)
    test.velocity = np.copy(velocity)
    test.fly_from_nearby(100)
    
    assert((test.velocity == fixture).all(), "fly_from_nearby does not give correct velocities for radius = 100")
    
        
        
    
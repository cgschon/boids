#!/usr/bin/env python3
"""
Refactorisation of boids package. This file contains the 'boids' class
which:
    - initialises the boids by number of boids and x/y direction
    starting positions and velocitites
    -contains methods for 3 main characteristics of boids' movement:
        -fly_middle with a weighting 
        -fly_from_nearby
        -match_nearby_speed
"""
import numpy as np
import scipy.spatial



class boids(object):
    """boids class: class for initialising a set of boids for the animations run.
    contains methods for the boids' movement characteristics. User initialises the
    number of boids (int), x and y positions and velocity ranges (list length 2), 
    (all have defaults)"""
    def __init__(self, num_boids = 50, x_pos_range = [-450,50], y_pos_range = [300,600], x_vel_range = [0,10], y_vel_range = [-20,20]):
        self.position = [x_pos_range[1] + (x_pos_range[0]-x_pos_range[1])*np.random.rand(num_boids),\
                         y_pos_range[1] + (y_pos_range[0]-y_pos_range[1])*np.random.rand(num_boids)]
        self.velocity = [x_vel_range[1] + (x_vel_range[0]-x_vel_range[1])*np.random.rand(num_boids),\
                         y_vel_range[1] + (y_vel_range[0]-y_vel_range[1])*np.random.rand(num_boids)]
        self.num_boids = num_boids
        
    def fly_middle(self, weight):
        for v, p in zip(self.velocity, self.position):
            for j, vs in enumerate(v):
                for ps in p:
                    vs += (ps-p[j])*weight/self.num_boids
                v[j] = vs

    
    def make_dist_mat(self):
         dists = scipy.spatial.distance.pdist(np.array(self.position).transpose(), 'sqeuclidean')
         dist_mat = scipy.spatial.distance.squareform(dists)
         return(dist_mat)
        
    def fly_from_nearby(self, radius):
         #take positions and velocities, looping simulateously through both
         dist_mat = self.make_dist_mat()
         for i, (v,p) in enumerate(zip(self.velocity, self.position)):
             for j, vs in enumerate(v):
                 if(dist_mat[i][j] < radius):
                     v += p[i] - p[j]
    
    def match_nearby_speed(self, radius, weight):
        dist_mat = self.make_dist_mat()
        for i, (v,p) in enumerate(zip(self.velocity, self.position)):
             for j, vs in enumerate(v):
                 if(dist_mat[i][j] < radius):
                     v += (v[j] - v[i])*weight/self.num_boids

    def move(self):
        for v, p in zip(self.velocity, self.position):
            p += v 
            
    def update(self, mid_weight = 0.01, nearby_radius = 100, speed_radius = 10000, speed_weight = 0.125):
        """update positions in one go using the boids methods"""
        self.fly_middle(mid_weight)
        self.fly_from_nearby(nearby_radius)
        self.match_nearby_speed(speed_radius, speed_weight)
        self.move()
                    

        
        
    
        



#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 17 18:11:41 2017

@author: ChristopherSchon
"""
from Boids.boids import boids as boids
from matplotlib import pyplot as plt
from matplotlib import animation

class visualise_boids(boids):
    """Class based approach to the boids animation"""
    
    def __init__(self, frames = 50, interval = 50, num_boids = 50, x_pos_range = [-450,50], y_pos_range = [300,600], \
                 x_vel_range = [0,10], y_vel_range = [-20,20], xlims = (-500,1500), ylims = (-500, 1500), \
                 mid_weight = 0.01, nearby_radius = 100, speed_radius = 10000, speed_weight = 0.125):
        
        boids.__init__(self, num_boids, x_pos_range, y_pos_range, x_vel_range, y_vel_range)
        
        self.frames = frames
        self.interval = interval
        self.xlims = xlims
        self.ylims = ylims
        self.mid_weight = mid_weight
        self.nearby_radius = nearby_radius
        self.speed_radius = speed_radius
        self.speed_weight = speed_weight

        
    def animate(self, frame):
        self.update(self.mid_weight, self.nearby_radius, self.speed_radius, self.speed_weight)
        self.scatter.set_offsets(list(zip(self.position[0],self.position[1])))
        
    def run_animation(self):
        self.figure = plt.figure()
        self.axes = plt.axes(xlim=self.xlims, ylim=self.ylims)
        self.scatter = self.axes.scatter(self.position[0],self.position[1])
        self.anim = animation.FuncAnimation(self.figure, self.animate, frames=self.frames, interval=self.interval)
        plt.show()
        